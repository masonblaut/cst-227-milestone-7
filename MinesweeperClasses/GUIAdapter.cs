﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MinesweeperClasses
{
    public class GUIAdapter
    {
        //Mason Blaut - This is my own work

        static private Board myBoard;
        static private int boardSize;
        private String playerName;
        private int playerScore = 0;
        private int playerTime;

        public GUIAdapter(Board theBoard, int theBoardSize)
        {
            myBoard = theBoard;
            boardSize = theBoardSize;
        }

        public void visitSquare(int row, int col)
        {

            if (myBoard.theGrid[row, col].hasBomb)
            {
                myBoard.theGrid[row, col].visited();
            }
            else
            {
                noBombFlood(row, col);
            }
        }

        public void noBombFlood(int x, int y)
        {
            if (x < 0 || x > boardSize - 1 || y < 0 || y > boardSize - 1 ||
                myBoard.theGrid[x, y].isVisited == true)
            {
                return;
            }

            myBoard.theGrid[x, y].visited();
            playerScore += 50;

            if (myBoard.theGrid[x, y].nextCount == 0)
            {
                noBombFlood(x - 1, y);
                noBombFlood(x, y - 1);
                noBombFlood(x, y + 1);
                noBombFlood(x + 1, y);
            }

        }

        public int getPlayerScore()
        {
            return playerScore;
        }

        public String getPlayerName()
        {
            if (playerName.Length > 1)
            {
                return playerName;
            }
            else
            {
                return "No Name";
            }
        }

        public int getPlayerTime()
        {
            return playerTime;
        }

        public void setPlayerName(String name)
        {
            playerName = name;
        }

        public void setPlayerTime(int time)
        {
            playerTime = time;
        }

        public Board getMyBoard()
        {
            return myBoard;
        }

        public int getBoardSize()
        {
            return boardSize;
        }
    }
}
